using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public void LoadScene()
    {
        // Получаем индекс сцены и загружаем сцену с индексом на один больше
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
